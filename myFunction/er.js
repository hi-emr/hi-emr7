/**
 * Created by User on 8/9/2560.
 */

/******************ER*******************/
function getErData() {
    var vn = vn_global_var;
    console.log(vn);
    //alert(vn);
    $('div#er_result').empty();
    if (vn) {
        $.get('get_emergency_data.php', { vn: vn }, function(data) {
            console.log(data);
            $("div#er_result").html(data);
        });
    }

}

function getErProcedure() {
    var vn = vn_global_var;

    $('div#er_procedure').empty();
    if (vn) {
        $.get('get_proc_er_data.php', { vn: vn }, function(data) {
            console.log(data);
            $("#er_procedure").html(data);
        });
    }

}